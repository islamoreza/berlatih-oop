<?php

require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");

$sheep = new Animal ("shaun");
echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs ."<br>";
echo "Cold Blooded : ". $sheep->cold_blooded . "<br><br>";

$kodok = new Frog  ("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs ."<br>";
echo "Cold Blooded : ". $kodok->cold_blooded . "<br>";
$kodok->jump();

$kera = new Ape ("kera sakti");
echo "Name : " . $kera->name . "<br>";
echo "Legs : " . $kera->legs ."<br>";
echo "Cold Blooded : ". $kera->cold_blooded . "<br>";
$kera->yell();


?>